set nocompatible            " disable vi compatibility
set background=dark         " set dark background
set backup                  " just to be safe
set cursorline              " highlight current line
set laststatus=2            " always show file status line
set number                  " show line numbers
set ruler                   " show ruler
set showmatch               " show matching brackets/parantheses
set spell                   " enable spell checking
set term=$TERM              " set terminal

" Searching
set hlsearch                " highlight search terms
set ignorecase              " search is case insensitive
set smartcase               " ... unless using a capital letter

" Indenting & tabs/spaces
set autoindent              " indent at same depth than previous line
set expandtab               " Replace tabs with spacess
set tabstop=4               " number of spaces per tab
set shiftwidth=4            " re-indent with
set softtabstop=4           " number of columns to jump when hitting tab

" Force utf-8
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
scriptencoding utf-8

" Auto-detect file types
filetype plugin indent on

" Enable sytax highlighting
syntax on

" Dark theme
colorscheme solarized
