# Color aliases
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

# Load the shell completions:
for file in $(brew --prefix)/etc/bash_completion.d/*; do
  [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# Grunt bash-completion
eval "$(grunt --completion=bash)"

# Load the shell dotfiles, and then some:
for file in ~/.{exports,aliases,functions,extra,work}; do
  [ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# NVM config
source $(brew --prefix nvm)/nvm.sh
export NVM_DIR=~/.nvm

# Prompt
PS1='\[${CYAN}\]\u\[${WHITE}\]:\[${GREEN}\]\w\[${MAGENTA}\]`__git_ps1 " \[${WHITE}\](\[${MAGENTA}\]%s\[${WHITE}\])"`\[${WHITE}\]\r\n\[${NORMAL}\]\$ '

