#!/bin/bash

######################
# Update the machine #
######################
NOTIFICATION_TITLE="Env update"

source ~/.bash_profile

sudo -v



# Brew
######

notify "Updating homebrew..." $NOTIFICATION_TITLE
brew update
notify "Upgrading tools..." $NOTIFICATION_TITLE
brew upgrade
brew cleanup



# Node modules
##############

notify "Upgrading node modules..." $NOTIFICATION_TITLE
nvm use stable
sudo npm update -g



# Update work files
###################

work-update
