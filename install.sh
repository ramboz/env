#!/bin/bash

########################
# Setup up the machine #
########################
NOTIFICATION_TITLE="Env Install"


# Retrieve the repository
#########################

# Install homebrew if not present
if [ -z "$(brew -v 2> /dev/null)" ]; then
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi
brew doctor

# Install git if it is not installed
if [ -z "$(brew list|grep ^git$)" ]; then
    brew install git
fi
if [ -z "$(git config user.name)" ]; then
    read -e -p "Enter your git name (i.e. John Smith): " GIT_USER
    git config --global user.name "$GIT_USER"
fi
if [ -z "$(git config user.email)" ]; then
    read -e -p "Enter your git email (i.e. john@smith.com): " GIT_EMAIL
    git config --global user.email "$GIT_EMAIL"
fi

# Clone the repo into ~/.env if it does not exist
cd
if [ ! -x ".env" ]; then
    git clone git@git.corp.adobe.com:ramboz/env.git .env
fi
cd .env

# Load the bash profile to have access to all the good stuff in the installer
source ~/.bash_profile


# Set up dotfiles and default directories
#########################################

notify "Setting up dotfiles and directories..." $NOTIFICATION_TITLE

# Symlink configuration files
cd dotfiles
for f in .*; do
    if [ "$f" != "." ] && [ "$f" != ".." ] && [ "$f" != .*.xml ] && [ "$f" != .*.swp ]; then
        ABS_PATH=`echo $(cd $(dirname $f); pwd)/$(basename $f)`
        # Copy directories so potential new files are not added to the repo
        if [ -d $1 ]; then
            cp -R $ABS_PATH ~/
        # but, symlink the files for quicker update
        elif [ -f $1 ]; then
            ln -sfF $ABS_PATH ~/
        fi
    fi
done

# Create default directories
mkdir -p $PROJECTS_PATH
mkdir -p ~/Documents/Training

# Install default tools & apps
##############################
cd ~/.env

# Get admin rights once
sudo -v

# Brew
notify "Installing tools..." $NOTIFICATION_TITLE
brew tap homebrew/completions
brew install homebrew/versions/bash-completion2
brew install caskroom/cask/brew-cask
brew install node
brew install nvm

# Brew Casks
notify "Installing apps..." $NOTIFICATION_TITLE
brew tap caskroom/versions

# Brew Casks: Tools
brew cask install caffeine
brew cask install grandperspective
brew cask install the-unarchiver

# Brew Casks: Dev
brew cask install iterm2
brew cask install sourcetree
brew cask install sublime-text3
brew cask install virtualbox

# Brew Casks: Browsers
brew cask install firefox-nightly
brew cask install firefoxdeveloperedition
brew cask install google-chrome
brew cask install google-chrome-canary
brew cask install opera-developer

# Brew Casks: Other
brew cask install adobe-creative-cloud
brew cask install skype
brew cask install spotify
brew cask install vlc

brew cleanup

# Node modules
notify "Installing node modules..." $NOTIFICATION_TITLE
sudo npm install -g bower
sudo npm install -g grunt-cli

# NVM
notify "Setting up nvm..." $NOTIFICATION_TITLE
nvm install stable


# Configure apps
################

# Copy application preferences
cp ~/.env/apps/* ~/Library/

# Install Sublime Text 3 Package Control
notify "Installing ST3 package control..." $NOTIFICATION_TITLE
mkdir -p ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/
curl -so ~/Library/Application\ Support/Sublime\ Text\ 3/Packages/User/Package\ Control.sublime-package https://packagecontrol.io/Package%20Control.sublime-package



# Install work files
####################

work-install

notify "All set!" $NOTIFICATION_TITLE
