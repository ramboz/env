Environment Setup
=================

Sets up the default environment settings for the machine.

## Install

curl https://bitbucket.org/ramboz/env/raw/master/install.sh | sh

or

Download archive from: https://bitbucket.org/ramboz/env
Extract to ~/.env
Run ~/.env/install.sh

## Update

Run ~/.env/update.sh
